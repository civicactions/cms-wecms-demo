# CMS-WECMS-DEMO

## Table of Contents

* [Requirements](#requirements)
* [Sandbox Instructions](#sandbox-instructions)
* [Starting Work](#starting-work)
* [After Development](#after-development)
* [Testing](#testing)
* [Docker Image Development](#docker-image-development)
* [Deployment](#deployment)

## Requirements

* Git - https://git-scm.com/downloads
* Docker - https://www.docker.com/community-edition
* Docker Compose

## Sandbox Instructions

### Initial setup
Run the following:

```
git clone git@gitlab.com:civicactions/cms-wecms-demo.git
cd cms-wecms-demo/
docker-compose up -d
```

The sandbox takes a minute or two to spin up (depending on your machine). You can follow progress by running `docker-compose logs`.

Once started, the site will be accessible at http://localhost:8118
 
You can [log into Drupal](http://localhost:8118/user) with the default credentials:
 * Username: `admin`
 * Password: `CMS12345`

To use CLI tools (composer, drush, drupal) run:

```
. bin/activate
```

### Stopping the sandbox:

```
docker-compose stop
```

### Recreating your sandbox from scratch:
```
docker-compose down -v
docker-compose up --build -d
```

## Starting Work

Work to be done in the project should be defined as _Issues_ within the GitLab project (https://gitlab.com/civicactions/cms-wecms-demo/issues).  The most streamlined way to start work is to:

* Select the issue to be worked on
* Cick the 'Create merge request' button
* Click the 'Check out branch'
* Cick the copy button for 'Step 1' and execute it within the workspace directory in your terminal.

The above will automatically create a new Feature Branch (FB) that corresponds to the Issue. It will also create a Merge Request (MR) that is useful to see the work and discussion pertaining to this issue.  The status of the pipeline execution related to the FB is also displayed.  Once the work is complete and approved, the MR can then be approved and merged into the Master branch.


### After Development

Make sure you export any configuration changes you've made to ensure that what you've done on your machine can be replicated on other machines. Do this by running `drush -y config:export` and commit those results before you submit your pull-request.

We initially used the [Default Content module](https://www.drupal.org/project/default_content) to deploy our placeholder content. See the custom module [README.md](web/modules/custom/wecms_content/README.md) file for details on exporting content to code.

## Testing

All testing tools run inside Docker, just make sure your terminial is activated (`. bin/activate`) and you should be good to go.

All of the test tools below are run for all commits pushed to GitLab, and all tests must pass before the code can be merged.

To run [PHPunit](https://phpunit.de/) unit tests:
```
phpunit
```

### Accessibility Testing

A suite of WCAG 2.0 and Section 508 accessibility tests can by run using [Pa11y](http://pa11y.org/):
```
pa11y
```

### Security Testing

[OWASP Zed Attack Proxy (ZAP)](https://www.owasp.org/index.php/OWASP_Zed_Attack_Proxy_Project) can run scans against the site. By default it will run a 1 minute baseline passive scan, but you can pass in arguments to run other scans as needed.
```
zap
```

## Docker Image Development

If you have not already done so, you will need to login to the gitlab registry with docker:

```
docker login registry.gitlab.com
```

Edit the Dockerfile, then rebuild using:

```
docker build -t registry.gitlab.com/civicactions/cms-wecms-demo/master .
```

Once complete, you may update the registry with the new image:
```
docker push registry.gitlab.com/civicactions/cms-wecms-demo/master
```

# Deployment

This GitLab project is integrated with an AWS Kubernetes cluster.  The GitLab pipeline is configured to automatically deploy Feature Branches to their own dedicated environments.  When Feature Branches are approved and merged into the Master Branch, a GitLab Pipeline is triggered to run against the latest version of the Master Branch.  This Pipeline execution automatically packages, tests, and deploys the latest version of the project to the production environment.


