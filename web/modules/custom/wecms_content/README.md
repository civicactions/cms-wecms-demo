# WECMS content
This module provides helper functions for the WECMS demo site. It is also used to hold content exported by the _Default Content_ module.

### Exporting default content
1. Create content and users via the UI.
1. Get the _UUID_ for the different data types:
    1. Nodes: `drush sqlq 'select uuid from node where 1'`
    1. Users: `drush sqlq 'select uuid from users where uid > 1'`
    1. Paragraphs: `drush sqlq 'select uuid from paragraphs_item where 1'`
    1. Blocks: `drush sqlq 'select uuid from block_content where 1'`
1. Copy the UUIDs to the **wecms_content.info.yml** file. 

    For example:

    ~~~~
    default_content:  
      node:
        - 2444a955-6b68-4bd7-8426-02ed84df344e
        - ...
      user:
        - 4f4dc99a-e9fd-448b-bca8-c205da565ab5
        - ...
      paragraph:
        - ba1a54f6-cc44-47a7-bb85-c085964a5b36
        - ...
      block_content:
        - b116293b-c0aa-4874-a6f1-be192da1fe64
        - ...
    ~~~~
1. Once you have added the UUIDs to the _.info.yml_ file, run 
    
        drush dcem wecms_content
     
   to create the **json** files that will be used to create the content. The files will live in the `/content/node/` and the `/content/user/` directories within this module.
1. The content and users will be created the next time that the site is built.

