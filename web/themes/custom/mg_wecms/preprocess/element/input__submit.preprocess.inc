<?php

/**
 * @file
 * Preprocess function for this hook.
 */

/**
 * Implements hook_preprocess_input__textfield().
 */
function mg_wecms_preprocess_input__submit(&$variables) {
  $input_submit_classes = &$variables['attributes']['class'];
  if (($key = array_search('usa-button', $input_submit_classes)) !== false) {
    unset($input_submit_classes[$key]);
  }
  $variables['attributes']['class'][] = 'ds-c-button ds-c-button--primary';
}
