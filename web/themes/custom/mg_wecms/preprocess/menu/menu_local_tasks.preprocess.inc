<?php

/**
 * @file
 * Preprocess function for this hook.
 */

/**
 * Implements hook_preprocess_menu_local_tasks().
 */
function mg_wecms_preprocess_menu_local_tasks(&$variables) {
  if (!empty($variables['primary'])) {
    foreach ($variables['primary'] as $menu_item_key => $menu_attributes) {

      // Remove unwanted USWDS button classes and use CMSDS classes instead.
      $menu_link_classes = &$variables['primary'][$menu_item_key]['#link']['localized_options']['attributes']['class'];
      $remove_class = array('usa-button', 'usa-button--active');
      foreach ($remove_class as $class) {
         if (($key = array_search($class, $menu_link_classes)) !== false) {
          unset($menu_link_classes[$key]);
         }
      }

      $variables['primary'][$menu_item_key]['#link']['localized_options']['attributes']['class'][] = 'ds-c-button ds-c-button--primary';
      if (!empty($variables['primary'][$menu_item_key]['#active'])) {
        $variables['primary'][$menu_item_key]['#link']['localized_options']['attributes']['class'][] = 'ds-c-button--active';
      }
    }
  }

  if (!empty($variables['secondary'])) {
    foreach ($variables['secondary'] as $menu_item_key => $menu_attributes) {
      $variables['secondary'][$menu_item_key]['#link']['localized_options']['attributes']['class'][] = '';
      $variables['secondary'][$menu_item_key]['#link']['localized_options']['attributes']['class'][] = 'ds-c-button';
      $variables['secondary'][$menu_item_key]['#link']['localized_options']['attributes']['class'][] = 'ds-c-button--primary';
      if (!empty($variables['secondary'][$menu_item_key]['#active'])) {
        $variables['secondary'][$menu_item_key]['#link']['localized_options']['attributes']['class'][] = 'ds-c-button--active';
      }
    }
  }
}
