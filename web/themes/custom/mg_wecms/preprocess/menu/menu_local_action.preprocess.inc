<?php

/**
 * @file
 * Preprocess function for this hook.
 */

/**
 * Implements hook_preprocess_menu_local_action().
 */
function mg_wecms_preprocess_menu_local_action(&$variables) {
  $variables['link']['#options']['attributes']['class'][] = 'ds-c-button ds-c-button--primary';
  $variables['link']['#options']['attributes']['class'][] = 'ds-c-button';
}
