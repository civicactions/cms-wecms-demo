<?php

/**
 * @file
 * Preprocess function for this hook.
 */

/**
 * Implements hook_preprocess_page().
 */
function mg_wecms_preprocess_page(&$variables) {
  // Set the path to the base theme.
  $variables['active_theme_path'] = base_path() . \Drupal::theme()->getActiveTheme()->getPath();

  // Decide on the type of header.
  $header_style = theme_get_setting('mg_wecms_header_style');
  if (empty($header_style)) {
    $header_style = 'extended';
  }
  $variables['header_style'] = 'ds-c-header--' . $header_style;
  $variables['header_classes'] = 'ds-c-header ' . $variables['header_style'];

  // And some helpful flags for the header.
  $variables['header_basic'] = ('basic' === $header_style);
  $variables['header_extended'] = ('extended' === $header_style);

  // Hide the secondary menu if using the basic header.
  if ($variables['header_basic']) {
    $variables['page']['secondary_menu'] = FALSE;
  }

  // Show the official U.S. Government banner?
  if (theme_get_setting('mg_wecms_government_banner')) {
    $variables['government_banner'] = [
      '#theme' => 'government_banner',
      '#image_base' => $variables['active_theme_path'] . '/assets/img',
    ];
  }

  // Decide on the type of footer.
  $footer_style = theme_get_setting('mg_wecms_footer_style');
  if (empty($footer_style)) {
    $footer_style = 'slim';
  }
  $variables['footer_style'] = 'ds-c-footer--' . $footer_style;
  $variables['footer_classes'] = 'ds-c-footer ' . $variables['footer_style'];
  $variables['footer_menu_width'] = 'mobile-lg:grid-col-12';
  $variables['footer_menu_padding'] = '';
  $variables['footer_logo_gap'] = 'grid-gap';

  // Add some helpful flags for the footer.
  $variables['footer_slim'] = ('slim' == $footer_style);
  $variables['footer_medium'] = ('medium' == $footer_style);
  $variables['footer_big'] = ('big' == $footer_style);

  // Some differences for the slim footer.
  if ($variables['footer_slim']) {
    $variables['footer_menu_padding'] = 'desktop:padding-x-4';
    $variables['footer_logo_gap'] = 'grid-gap-2';
    if ($variables['email'] || $variables['phone']) {
      $variables['footer_menu_width'] = 'mobile-lg:grid-col-8';
    }
  }

  // Whether to display the primary footer section.
  $display_primary_footer = (!empty($variables['page']['footer']));
  $display_primary_footer |= (!empty($variables['page']['footer_menu']));
  $display_primary_footer |= ($variables['footer_slim'] && ($variables['email'] || $variables['phone']));
  $variables['display_footer_primary'] = $display_primary_footer;

  // Whether to display the secondary footer section.
  $display_secondary_footer = (!empty($variables['page']['footer_secondary']));
  $display_secondary_footer |= $variables['display_footer_agency'];
  $variables['display_footer_secondary'] = $display_secondary_footer;
}
