<?php

/**
 * @file
 * Preprocess function for this hook.
 */

/**
 * Implements hook_preprocess_form_element().
 */
function uswds_preprocess_form_element(&$variables) {
  $variables['attributes']['class'][] = 'usa-form-group';

  if (!empty($variables['errors'])) {
    $variables['error_id'] = $variables['element']['#attributes']['id'];
    $variables['attributes']['class'][] = 'usa-form-group--error';
  }
}
