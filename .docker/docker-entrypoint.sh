#!/bin/bash -e
set -e

# Set the apache user and group to match the host user.
# Optionally use the HOST_USER env var if provided.
if [ "$HOST_USER" ]; then
  OWNER=$(echo $HOST_USER | cut -d: -f1)
  GROUP=$(echo $HOST_USER | cut -d: -f2)
else
  OWNER=$(stat -c '%u' /app/src)
  GROUP=$(stat -c '%g' /app/src)
fi
if [ "$OWNER" != "0" ]; then
  usermod -o -u $OWNER www-data
  groupmod -o -g $GROUP www-data
fi
usermod -s /bin/bash www-data
usermod -d /app/src www-data

echo "The apache user and group has been set to the following:"
id www-data

# Wait for mysql connection if not in Gitlab Kubernetes environment
if [ -z ${GITLAB_ENVIRONMENT_NAME+x} ]; 
  then echo "We're out outside of GitLab Kubernetes environment";

  echo "Connecting to database..."
  i=0
  while ! bash -c "echo 2>/dev/null > /dev/tcp/db/3306"; do
    i=$(($i+1))
    if [ "$i" -gt 30 ]; then
      echo "Cannot connect to database. Make sure it is running and the web container is linked to it."
      exit 1;
    fi
    sleep 2;
  done
else
  # Workaround name resolution issue with Gitlab kubernetes
  export DATABASE_HOST=$(env |grep DB_SERVICE_HOST|cut -d '=' -f 2) 
  echo "$DATABASE_HOST     db" >> /etc/hosts
fi
echo "Connected to database!"

# Initally we didn't have a prod DB to use, so we reinstalled Drupal locally.
#if [[ "$(drush core-status --field=bootstrap | sed 's/[^a-zA-Z]*//g')" == "Successful" ]]; then
#  echo "Drupal already installed"
#  su www-data -c'bash /app/src/.docker/drupal-install.sh skip-install'
#else
#  su www-data -c'bash /app/src/.docker/drupal-install.sh'
#fi

# Since we now have a prod DB, skip the installation step.
su www-data -c'bash /app/src/.docker/drupal-install.sh skip-install'

echo "Starting Apache"
exec /usr/local/bin/docker-php-entrypoint apache2-foreground
