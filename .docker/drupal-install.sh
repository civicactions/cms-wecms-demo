#!/bin/bash -e
set -e
DRUSH=/app/src/vendor/bin/drush
DRUPAL=/app/src/vendor/bin/drupal
PASSWORD="CMS12345"

if [ "${1}" != "skip-install" ]; then
  echo "Installing Drupal"
  $DRUSH -y site:install minimal --account-pass=${PASSWORD} --sites-subdir=default --db-url=mysql://dbuser:dbpass@db:3306/drupal --config-dir=/app/src/config/sync
fi

echo "Importing Configuration"
$DRUSH -y config-import

echo "Setting passwords"
for NAME in $($DRUSH sqlq "SELECT name FROM users_field_data WHERE uid > 0"); do
  $DRUSH user:password "${NAME}" "${PASSWORD}"
  echo "Default Username is: ${NAME}"
  echo "Default Password is: ${PASSWORD}"
done

echo "Rebuilding node access"
$DRUPAL --root=/app/src/docroot node:access:rebuild

echo "Rebuilding cache"
$DRUSH cache:rebuild

echo "Indexing content for Search"
$DRUSH search-api:reset-tracker
$DRUSH search-api:index
