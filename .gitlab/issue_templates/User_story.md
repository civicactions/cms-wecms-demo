**User story**

As a [persona] i want to [some goal], so that [some reason].

**Acceptance criteria**

1. [this thing is true]
1. [that thing is true]

**Testing**

* [this works]
* [that works]

**Implementation steps**

- [ ] this thing
- [ ] that thing
  - [ ] that sub-thing


/label ~WECMS ~userstory